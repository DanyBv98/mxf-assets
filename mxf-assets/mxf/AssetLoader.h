#pragma once

#include <fstream>
#include <string>
#include <vector>
#include "dll.h"

namespace mxf
{
	class MXF_ASSETS_API AssetLoader
	{
	private:
		struct FileMetadata {
			FileMetadata(const std::string& name, uint32_t size, uint32_t crc, std::streampos dataPosition = std::streampos(-1)) :
				name(name), size(size), crc(crc), dataPosition(dataPosition) {}

			const std::string name;
			const uint32_t size;
			const uint32_t crc;

			std::streampos dataPosition;
		};


	public:
		void load(const std::string& filePath);
		std::vector<char> loadAsset(const std::string& name);
		std::vector<char> loadAsset(const FileMetadata& file);
		void extract(const std::string& outputPath);
		const std::vector<FileMetadata>& getFiles() const;

	private:
		template <typename T>
		T read();

		template <typename T>
		void read(T* buffer, size_t size);

	private:
		std::ifstream _inputFile;
		std::vector<FileMetadata> _filesData;

		static const uint32_t bufferSize = 1048576;
	};
}


template<typename T>
inline T mxf::AssetLoader::read()
{
	constexpr size_t typeSize = sizeof(T);
	char buffer[typeSize];

	this->_inputFile.read(buffer, typeSize);
	return *reinterpret_cast<T*>(buffer);
}

template<typename T>
inline void mxf::AssetLoader::read(T* buffer, size_t size)
{
	this->_inputFile.read(reinterpret_cast<char*>(buffer), size);
}
