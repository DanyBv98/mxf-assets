#pragma once
#include <vector>
#include <string>
#include <fstream>
#include "dll.h"

namespace mxf
{
	class MXF_ASSETS_API AssetPacker
	{
	public:
		void addFile(const std::string& path, std::string alias = "");
		void compile(const std::string& ouputPath);

	private:
		struct FileMetadataPosition {
			std::streampos fileSize;
			std::streampos crc;
		};

		struct FileInfo {
			FileInfo(const std::string& path, const std::string& alias) : path(path), alias(alias) {}

			std::string path;
			std::string alias;
		};


		const static int bufferSize = 1048576;

		template <typename T>
		static void write(std::ofstream& outputStream, T object, size_t size = 0, typename std::enable_if<std::is_const<T>::value>::type* = 0);

		template <typename T>
		static void write(std::ofstream& outputStream, T object, size_t size = 0, typename std::enable_if<std::is_pointer<T>::value>::type* = 0);

		template <typename T>
		static void write(std::ofstream& outputStream, T object, typename std::enable_if<!std::is_pointer<T>::value>::type* = 0);

		std::vector<FileInfo> _fileList;
	};

	template<typename T>
	inline void AssetPacker::write(std::ofstream& outputStream, T object, size_t size, typename std::enable_if<std::is_const<T>::value>::type*)
	{
		write(outputStream, const_cast<std::remove_const_t<T>>(object), size);
	}

	template<typename T>
	inline void AssetPacker::write(std::ofstream& outputStream, T object, size_t size, typename std::enable_if<std::is_pointer<T>::value>::type*)
	{
		outputStream.write(reinterpret_cast<const char*>(object), size * sizeof(*object));
	}

	template<typename T>
	inline void AssetPacker::write(std::ofstream& outputStream, T object, typename std::enable_if<!std::is_pointer<T>::value>::type*)
	{
		write(outputStream, &object, 1);
	}

}
