#pragma once
#include "AssetLoader.h"
#include "dll.h"
#include <set>
#include <memory>

namespace mxf
{
	class MXF_ASSETS_API MultiAssetLoader
	{
	public:
		void addAssetFile(const std::string& assetFilePath);
		std::vector<char> load(const std::string& filePath);

		std::set<std::string> getFileNames() const;
	private:
		std::vector<std::shared_ptr<AssetLoader>> _assetLoaders;
	};
}