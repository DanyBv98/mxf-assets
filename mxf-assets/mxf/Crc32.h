#pragma once

#include <stdint.h>

namespace mxf
{
	class Crc32
	{
	public:
		Crc32(uint32_t initialCrc = 0);
		void process_bytes(const void* data, uint32_t length);
		uint32_t get() const;
		void reset();
	private:
		uint32_t _currentCrc;
		static uint32_t _lookupTable[];
	};
}
