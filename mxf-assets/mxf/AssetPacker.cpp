#include "AssetPacker.h"
#include <iostream>
#include "Crc32.h"
#include <regex>

void mxf::AssetPacker::addFile(const std::string& filePath, std::string alias)
{
	if (alias.empty())
		alias = filePath;
	
	alias = std::regex_replace(alias, std::regex(R"(\\)"), R"(/)");
	this->_fileList.emplace_back(filePath, alias);
}

void mxf::AssetPacker::compile(const std::string& outputPath)
{
	std::ofstream outputFile(outputPath, std::ios::binary | std::ios::out);

	const uint32_t filesCount = this->_fileList.size();
	write(outputFile, filesCount);

	std::vector<FileMetadataPosition> metadataPositions;
	metadataPositions.resize(filesCount);

	for (uint32_t index = 0; index < filesCount; ++index)
	{
		const auto& file = this->_fileList[index];

		uint32_t aliasSize = file.alias.size();
		write(outputFile, aliasSize);
		write(outputFile, file.alias.c_str(), aliasSize);

		uint32_t fileSize = 0; // will be completed later by seeking
		metadataPositions[index].fileSize = outputFile.tellp();
		write(outputFile, fileSize);

		uint32_t crc = 0; // will be completed later by seeking
		metadataPositions[index].crc = outputFile.tellp();
		write(outputFile, crc);
	}

	std::vector<char> buffer;
	buffer.resize(bufferSize);

	for (uint32_t index = 0; index < filesCount; ++index)
	{
		const auto& file = this->_fileList[index];

		std::ifstream fileReader(file.path, std::ios::binary | std::ios::in);

		uint32_t fileSize = 0;

		Crc32 fileCrc;
		while (!fileReader.eof())
		{
			fileReader.read(buffer.data(), bufferSize);

			uint32_t bytesRead = bufferSize;
			if (fileReader.eof())
			{
				bytesRead = static_cast<uint32_t>(fileReader.gcount());
			}

			fileSize += bytesRead;

			fileCrc.process_bytes(buffer.data(), bytesRead);
			write(outputFile, buffer.data(), bytesRead);
		}

		auto currentPosition = outputFile.tellp();

		outputFile.seekp(metadataPositions[index].fileSize);
		write(outputFile, fileSize);

		outputFile.seekp(metadataPositions[index].crc);
		write(outputFile, fileCrc.get());

		outputFile.seekp(currentPosition);
	}
}
