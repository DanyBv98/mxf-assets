#pragma once

#ifdef MXF_ASSETS_EXPORTS
#define MXF_ASSETS_API __declspec(dllexport)
#else
#define MXF_ASSETS_API __declspec(dllimport)
#endif