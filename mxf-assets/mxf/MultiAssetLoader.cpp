#include "MultiAssetLoader.h"

void mxf::MultiAssetLoader::addAssetFile(const std::string& assetFilePath)
{
    auto loader = std::make_shared<AssetLoader>();
    loader->load(assetFilePath);
    _assetLoaders.push_back(loader);
}

std::vector<char> mxf::MultiAssetLoader::load(const std::string& filePath)
{
    for (auto& assetLoader : _assetLoaders)
    {
        auto result = assetLoader->loadAsset(filePath);
        if (!result.empty())
        {
            return result;
        }
    }
    return {};
}

std::set<std::string> mxf::MultiAssetLoader::getFileNames() const
{
    std::set<std::string> files;
    for (const auto& loader : _assetLoaders)
    {
        for (const auto& file : loader->getFiles())
        {
            files.insert(file.name);
        }
    }
    return files;
}
