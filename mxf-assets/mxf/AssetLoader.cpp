#include "AssetLoader.h"
#include <iostream>
#include <filesystem>
#include "Crc32.h"

namespace fs = std::filesystem;

void mxf::AssetLoader::load(const std::string& filePath)
{
	if (this->_inputFile.is_open())
		this->_inputFile.close();

	this->_inputFile.open(filePath, std::ios::in | std::ios::binary);

	const uint32_t filesCount = this->read<uint32_t>();
	this->_filesData.reserve(filesCount);

	for (uint32_t index = 0; index < filesCount; ++index)
	{
		const uint32_t nameSize = this->read<uint32_t>();
		std::string name;
		name.resize(nameSize);

		this->read<char>(name.data(), nameSize);

		const uint32_t fileSize = this->read<uint32_t>();
		const uint32_t fileCrc = this->read<uint32_t>();
		this->_filesData.emplace_back(name, fileSize, fileCrc);
	}

	for (auto& file : this->_filesData)
	{
		file.dataPosition = this->_inputFile.tellg();
		this->_inputFile.seekg(file.dataPosition + std::streamoff(file.size));
	}

	this->_inputFile.seekg(this->_filesData[0].dataPosition);
}

std::vector<char> mxf::AssetLoader::loadAsset(const std::string& name)
{
	auto iterator = std::find_if(_filesData.begin(), _filesData.end(), [name](const AssetLoader::FileMetadata& fileData) { return fileData.name == name; });
	if (iterator == _filesData.end()) {
		return {};
	}
	return loadAsset(*iterator);
}

std::vector<char> mxf::AssetLoader::loadAsset(const FileMetadata& file)
{
	this->_inputFile.seekg(file.dataPosition);
	std::vector<char> data;
	data.resize(file.size);
	this->read<char>(data.data(), file.size);

	Crc32 crc;
	crc.process_bytes(data.data(), data.size());
	if (crc.get() != file.crc)
	{
		std::cerr << "WARNING: File " << file.name << " failed the CRC check!";
	}
	return data;
}

const std::vector<mxf::AssetLoader::FileMetadata>& mxf::AssetLoader::getFiles() const
{
	return this->_filesData;
}

void mxf::AssetLoader::extract(const std::string& outputPath)
{
	std::vector<char> buffer;
	buffer.resize(bufferSize);

	fs::path outputDirectory(outputPath);
	for (auto& file : _filesData)
	{
		this->_inputFile.seekg(file.dataPosition);

		auto outputFilePath = outputDirectory / file.name;
		fs::create_directories(outputFilePath.parent_path());

		std::ofstream outputFile(outputFilePath, std::ios::out | std::ios::binary);

		uint32_t remainingSize = file.size;
		Crc32 calculatedCrc;
		while (remainingSize > 0)
		{
			uint32_t bytesToRead = std::min(remainingSize, bufferSize);
			this->read<char>(buffer.data(), bytesToRead);
			outputFile.write(buffer.data(), bytesToRead);
			calculatedCrc.process_bytes(buffer.data(), bytesToRead);
			remainingSize -= bytesToRead;
		}
		if (calculatedCrc.get() != file.crc)
		{
			std::cerr << "WARNING: File " << file.name << " failed the CRC check!";
		}
	}
}
