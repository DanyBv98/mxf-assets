#include <iostream>
#include <filesystem>
#include <argparse/argparse.hpp>

#include <mxf/AssetPacker.h>
#include <mxf/AssetLoader.h>
#include <mxf/MultiAssetLoader.h>

namespace fs = std::filesystem;

int main(int argc, char* argv[])
{
	argparse::ArgumentParser program("mxf-assetbundler");

	program.add_argument("input_path")
		.help("This should be a directory if packing or a content file if unpacking.");

	program.add_argument("output_path")
		.help("This should be a file path if packing or a directory path if unpacking.")
		.default_value(std::string(""));

	program.add_argument("--unpack")
		.help("Unpack the bundle instead of packing it")
		.default_value(false)
		.implicit_value(true);

	program.add_argument("--list-files")
		.help("Do not unpack, only print the information about the contained files.")
		.default_value(false)
		.implicit_value(true);

	try {
		program.parse_args(argc, argv);
	}
	catch (const std::runtime_error& err) {
		std::cerr << err.what() << std::endl;
		std::cerr << program;
		std::exit(1);
	}

	bool isListFiles = program.get<bool>("--list-files");
	bool isUnpacking = program.get<bool>("--unpack");
	std::string inputPath = program.get<std::string>("input_path");
	std::string outputPath = program.get<std::string>("output_path");

	if (!fs::exists(inputPath))
	{
		std::cerr << "The provided input does not exist." << std::endl;
		std::exit(1);
	}

	if (isListFiles || isUnpacking)
	{
		if (isListFiles && isUnpacking)
		{
			std::cerr << "--unpack cannot be used with --list-files" << std::endl;
			std::exit(1);
		}

		mxf::AssetLoader unpacker;
		unpacker.load(inputPath);

		if (isUnpacking)
		{
			if (outputPath.empty())
				outputPath = "assets";
			unpacker.extract(outputPath);
		}
		else
		{
			const auto& files = unpacker.getFiles();
			std::cout << files.size() << " files found in " << inputPath << std::endl;
			for (auto& file : files)
			{
				std::cout << "Found file: " << file.name << " with size " << file.size << " and CRC " << file.crc << "." << std::endl;
			}
		}
	}
	else
	{
		mxf::AssetPacker packer;
		for (const fs::directory_entry& dir_entry : fs::recursive_directory_iterator(inputPath))
		{
			if (dir_entry.is_regular_file())
			{
				auto& filePath = dir_entry.path();
				packer.addFile(fs::absolute(filePath).string(), fs::relative(filePath, inputPath).string());
			}
		}

		if (outputPath.empty())
			outputPath = "assets.mxa";
		packer.compile(outputPath);
	}

	return 0;
}